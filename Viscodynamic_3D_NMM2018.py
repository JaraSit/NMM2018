from fenics import *
import matplotlib.pyplot as plt
import math

# --------------------
# Define geometry
# --------------------
mesh = UnitCubeMesh(10, 10, 10)

plot(mesh, "Mesh")
plt.show()

# --------------------
# Define spaces
# --------------------
V = FunctionSpace(mesh, 'CG', 1)
W = VectorFunctionSpace(mesh, 'CG', 1)
Ten = TensorFunctionSpace(mesh, 'CG', 1)
s = TrialFunction(V)
q = TestFunction(V)
u = TrialFunction(W)
v = TestFunction(W)

# --------------------
# Parameters
# --------------------
start_time = 0.0
end_time = 8.0
tau = 0.05 # time increment

# Dimension of task
d = 3

# Material parameters
G_inf = 1.0
G_1 = 1.0
eta_1 = 0.5
rho = 2.0
nu = 0.49

G_unit = 1.0
lam, mu = (2*G_unit*nu/(1-2*nu), G_unit)

# Other constants
A = math.exp(-G_1/eta_1*tau)
B = eta_1*(1.0 - A)
C = 0.5*eta_1*(tau - B/G_1)

# --------------------
# Boundary conditions
# --------------------
class top(SubDomain):
	def inside(self,x,on_boundary):
		tol = 1e-10
		return abs(x[1]-1) < tol and on_boundary

class bottom(SubDomain):
	def inside(self,x,on_boundary):
		tol = 1e-10
		return abs(x[1]) < tol and on_boundary

# Instants of classes Top and Bottom
Top = top()
Bottom = bottom()

# Label top boundary as ds(1)
boundaries = MeshFunction("size_t", mesh, mesh.topology().dim() - 1) 
Top.mark(boundaries, 1)
ds = ds(subdomain_data=boundaries)

# Dirichlet boundary condition for bottom
DB_bottom =  DirichletBC(W, Constant((0.0, 0.0, 0.0)), Bottom)

# --------------------
# Methods
# --------------------
def epsilon(a):
	return sym(grad(a))

def sigma(a):
	return lam*tr(epsilon(a))*Identity(d) + 2.0*mu*epsilon(a)

# --------------------
# Initialization
# --------------------
u_old = Function(W)
u_ = Function(W)

d_u = Function(W)
dd_u = Function(W)
dd_u_old = Function(W)
u_bar = Function(W)
f_v = Function(Ten)

t = start_time
ff = Constant((0.0, 1.0, 0.0))

# --------------------
# XDMF output
# --------------------
# Create XDMF files for visualization output
xdmffile_u = XDMFFile('Results/disp.xdmf')

# Create progress bar
progress = Progress('Time-stepping')
set_log_level(PROGRESS)

# --------------------
# Main load loop
# --------------------
while t <= end_time:

	ff = Constant((0.0, 1.0, 0.0))

	# u_bar update
	u_bar.assign(u_old + d_u*tau + 0.25*dd_u*tau*tau)
	
	# Variational problem
	E_du = (inner(G_inf*epsilon(u) + A*f_v + B*epsilon(d_u) + C*epsilon(dd_u) + C*4/(tau*tau)*(epsilon(u) - epsilon(u_bar)), sigma(v)) \
		+ 4*rho/(tau*tau)*(inner(u, v) - inner(u_bar, v)))*dx \
		+ inner(Constant((0.0, 0.0, 0.0)), v)*dx \
		- inner(ff, v)*ds(1)

	# Solve linear variational problem
	solve(lhs(E_du) == rhs(E_du), u_, [DB_bottom])

	# Update of rest quantities
	f_v = project(A*f_v + B*epsilon(d_u) + C*epsilon(dd_u) + C*4/(tau*tau)*(epsilon(u_) - epsilon(u_bar)), Ten)
	u_old.assign(u_)
	dd_u_old.assign(dd_u)
	dd_u.assign(4/(tau*tau)*(u_-u_bar))
	d_u.assign(d_u + 0.5*(dd_u_old + dd_u)*tau)
	
	# Save solution to file (XDMF/HDF5)
	xdmffile_u.write(u_, t)

	# Update progress bar
	progress.update(t / end_time)

	# Increment of time
	t = t + tau